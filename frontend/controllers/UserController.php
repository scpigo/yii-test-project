<?php

namespace frontend\controllers;

use common\models\User;
use Yii;
use yii\web\Controller;

class UserController extends Controller
{
    Public $enableCsrfValidation = false;

    public function behaviors() {
    	$behaviors = parent::behaviors();

		$behaviors['authenticator'] = [
			'class' => \sizeg\jwt\JwtHttpBearerAuth::class,
		];

		return $behaviors;
	}

    public function actionIndex()
    {
        $user = Yii::$app->user->identity;

        return $this->asJson([
            'data' => [
					[
						'user' => $user,
					]
				],
            'statusText' => 'Успешно',
            'status' => 'ok'
        ]);
    }
}  