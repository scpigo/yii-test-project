<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;

class RedisController extends Controller
{
    Public $enableCsrfValidation = false;

    public function actionGet() {
        $key = Yii::$app->request->post('key');

        $value = Yii::$app->system_settings->getValue($key);

        if ($value) {
            return $this->asJson([
                'data' => [
                    'value' => $value
                ],
                'statusText' => 'Значение возвращено',
                'status' => 'ok'
            ]);
        } else {
            return $this->asJson([
                'data' => [],
                'statusText' => 'Произошла ошибка',
                'status' => 'error'
            ]); 
        }
    }

    public function actionClear() {
        Yii::$app->system_settings->clearCache();
        return $this->asJson([
            'data' => [],
            'statusText' => 'Отчищенно',
            'status' => 'ok'
        ]); 
    }
}