<?php

namespace frontend\controllers;

use common\models\User;
use Yii;
use yii\web\Controller;
use yii\httpclient\Client;

class AuthController extends Controller
{
    Public $enableCsrfValidation = false;

    public function behaviors() {
    	$behaviors = parent::behaviors();

		$behaviors['authenticator'] = [
			'class' => \sizeg\jwt\JwtHttpBearerAuth::class,
			'except' => [
				'register',
				'login',
				'refresh-token',
				'test',
				'options',
			],
		];

		return $behaviors;
	}

    private function generateJwt(\common\models\User $user) {
		$jwt = Yii::$app->jwt;
		$signer = $jwt->getSigner('HS256');
		$key = $jwt->getKey();
		$time = time();

		$jwtParams = Yii::$app->params['jwt'];

		return $jwt->getBuilder()
			->issuedBy($jwtParams['issuer'])
			->permittedFor($jwtParams['audience'])
			->identifiedBy($jwtParams['id'], true)
			->issuedAt($time)
			->expiresAt($time + $jwtParams['expire'])
			->withClaim('uid', $user->id)
			->getToken($signer, $key);
	}

	/**
	 * @throws yii\base\Exception
	 */
	private function generateRefreshToken(\common\models\User $user, \common\models\User $impersonator = null): \common\models\UserRefreshToken {
		$refreshToken = Yii::$app->security->generateRandomString(200);

		$userRefreshToken = new \common\models\UserRefreshToken([
			'urf_userID' => $user->id,
			'urf_token' => $refreshToken,
			'urf_ip' => Yii::$app->request->userIP,
			'urf_user_agent' => Yii::$app->request->userAgent,
			'urf_created' => gmdate('Y-m-d H:i:s'),
			'urf_expires' => gmdate('Y-m-d H:i:s', strtotime('+ 1 MONTH')),
		]);

		if (!$userRefreshToken->save()) {
			throw new \yii\web\ServerErrorHttpException('Failed to save the refresh token: '. $userRefreshToken->getErrorSummary(true));
		}

		return $userRefreshToken;
	}

	public function actionRegister() {
		$model = new \frontend\models\SignupForm();

		$model->load(Yii::$app->request->post(), '');

		if ($id = $model->signup()) {
			return $this->asJson([
				'data' => [
					'id' => $id
				],
				'statusText' => 'Регистрация прошла успешно',
				'status' => 'ok'
			]);
        }
		else
		{
			return $this->asJson([
				'data' => $model->getErrors(),
				'statusText' => 'Ошибка регистрации',
				'status' => 'error'
			]);
		}
	}

    public function actionLogin() {
		$model = new \common\models\LoginForm();

		if ($model->load(Yii::$app->request->post(), '') && $model->login())
		{
			$user = Yii::$app->user->identity;

			$access_token = $this->generateJwt($user);

			$refresh_token = $this->generateRefreshToken($user);

			return $this->asJson([
				'data' => [
					[
						'user_id' => $user->id,
						'access_token' => (string) $access_token,
						'refresh_token' => $refresh_token -> urf_token,
					]
				],
				'statusText' => 'Авторизация прошла успешно',
				'status' => 'ok'
			]);
		} else {
			return $this->asJson([
				'data' => $model->getErrors(),
				'statusText' => 'Ошибка авторизации',
				'status' => 'error'
			]);
		}
	}

    public function actionRefreshToken() {
		$refreshToken = Yii::$app->request->post("refreshToken");

		$userRefreshToken = \common\models\UserRefreshToken::findOne(['urf_token' => $refreshToken]);

		if (isset($userRefreshToken))
		{
			$date = gmdate('Y-m-d H:i:s');

			if ($date > $userRefreshToken->urf_expires)
			{
				return $this->asJson([
					'data' => [],
					'statusText' => 'Токен просрочен',
					'status' => 'error'
				]);
			}

			$user = \common\models\User::find()
					->where(['id' => $userRefreshToken->urf_userID])
					->one();

			$access_token = $this->generateJwt($user);

			$refresh_token = $this->generateRefreshToken($user);

			$userRefreshToken->delete();

			return $this->asJson([
				'data' => [
					[
						'user_id' => $user->id,
						'access_token' => (string) $access_token,
						'refresh_token' => $refresh_token -> urf_token,
					]
				],
				'statusText' => 'Токен обновлен',
				'status' => 'ok'
			]);
		}
		else
		{
			return $this->asJson([
				'data' => [],
				'statusText' => 'Токен не найден',
				'status' => 'error'
			]);
		}
	}

	public function actionLogout() {
		$refreshToken = Yii::$app->request->post("refreshToken");

		$userRefreshToken = \common\models\UserRefreshToken::findOne(['urf_token' => $refreshToken]);

		if (isset($userRefreshToken))
		{
			$date = gmdate('Y-m-d H:i:s');

			if ($date > $userRefreshToken->urf_expires)
			{
				return $this->asJson([
					'data' => [],
					'statusText' => 'Токен просрочен',
					'status' => 'error'
				]);
			}

			$userRefreshToken->delete();

			Yii::$app->user->logout();

			return $this->asJson([
				'data' => [],
				'statusText' => 'Выход из системы',
				'status' => 'ok'
			]);
		}
		else 
		{
			return $this->asJson([
				'data' => [],
				'statusText' => 'Токен не найден',
				'status' => 'error'
			]);
		}
	}
}

?>