<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use frontend\jobs\WriteJob;

class QueueController extends Controller
{
    Public $enableCsrfValidation = false;

    public function behaviors() {
    	$behaviors = parent::behaviors();

		$behaviors['authenticator'] = [
			'class' => \sizeg\jwt\JwtHttpBearerAuth::class,
			'except' => [
				'test',
			],
		];

		return $behaviors;
	}

    public function actionTest() {
        /* $id = Yii::$app->queueMicroservice->push(new WriteJob([
            'text' => 'not test ',
            'file' => Yii::$app->basePath . '/web/file.txt'
        ])); */

        $id = Yii::$app->queueMicroservice->push([
            'header' => 'Hello',
            'body' => 'Hello'
        ]);

        return $this->asJson([
            'data' => $id,
            'statusText' => 'Задание отправлено в очередь',
            'status' => 'ok'
        ]);
	}
}