<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\mongodb\Query;
use common\models\search\PostMetricsSearch;

class MongoController extends Controller
{
    Public $enableCsrfValidation = false;

    public function actionTest() {
        $searchModel = New PostMetricsSearch();
        $searchModel->load(Yii::$app->request->post(), '');

        $dataProvider = $searchModel->search();

        if ($dataProvider) {
            return $this->asJson([
                'data' => [
                    'count' => $dataProvider->getTotalCount(),
                    'items' => $dataProvider->getModels(),
                ],
                'statusText' => 'Метрики возвращены',
                'status' => 'ok'
            ]);
        }

        return $this->asJson([
            'data' => $searchModel->getErrors(),
            'statusText' => 'Ошибка',
            'status' => 'error'
        ]);
    }
}