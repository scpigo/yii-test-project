<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\Users;

class RegisterForm extends Model
{
    public $email;
    public $password;

    public function rules()
    {
        return [
            ['email', 'trim'],
            [['email', 'password'], 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\Users', 'message' => 'This email address has already been taken.'],

            ['password', 'string', 'min' => 6],
        ];
    }

    public function signup()
    {
        if (!$this->validate()) {
            return false;
        }
        
        $user = new Users();
        $user->email = $this->email;
        $user->setPassword($this->password);

        $result = $user->save();

        if ($result)
        {
            return $user->user_id;
        }
        return false;
    }
}