<?php

namespace frontend\jobs;

use yii\base\BaseObject;
use yii\queue\JobInterface;

class WriteJob extends BaseObject implements JobInterface
{
    public $file;
    public $text;

    public function execute($queue)
    {
        $text = time()."\n";
        file_put_contents($this->file, $text, FILE_APPEND);
    }
}