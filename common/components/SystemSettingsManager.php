<?php

namespace common\components;

use yii;
use yii\base\Component;
use common\models\SystemSettings;
use yii\helpers\ArrayHelper;

class SystemSettingsManager extends Component {
    private const CACHE_KEY = 'system_settings';

    public function getValue($key) {
        $redis = Yii::$app->redis;
        $cache = $redis->get(self::CACHE_KEY);
        $cache = json_decode($cache, true);

        if ($value = ArrayHelper::getValue($cache, $key)) {
            return $value;
        }

        $options = SystemSettings::find()->all();

        $options = ArrayHelper::map($options, 'key', 'value');

        $redis->set(self::CACHE_KEY, json_encode($options), 'EX', 60*60);
        
        return ArrayHelper::getValue($options, $key);
    }
    
    public function clearCache() {
        $redis = Yii::$app->redis;
        $redis->del(self::CACHE_KEY);
    }
}