<?php

use common\modules\calculator_service\src\Interfaces\CalculatorInterface;
use common\modules\calculator_service\src\Services\CalculatorService;

return [
        'definitions' => [
            CalculatorInterface::class => CalculatorService::class
        ]
    ];