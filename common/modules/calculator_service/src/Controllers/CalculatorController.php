<?php

namespace common\modules\calculator_service\src\Controllers;

use common\modules\calculator_service\src\Forms\CalculatorExecuteForm;
use Yii;
use yii\web\Controller;
use yii\di\Container;

class CalculatorController extends Controller
{
    Public $enableCsrfValidation = false;

    public function actionExecute() {

        $model = new CalculatorExecuteForm();
        $model->load(Yii::$app->request->post(), '');

		if ($result = $model->execute()) {
            return $this->asJson([
                'data' => [
                    'result' => $result,
                ],
                'statusText' => 'Действие выполнено',
                'status' => 'ok'
            ]);
        }

        return $this->asJson([
            'data' => $model->getErrors(),
            'statusText' => 'Ошибка выполнения операции',
            'status' => 'error'
        ]);
    }
}
