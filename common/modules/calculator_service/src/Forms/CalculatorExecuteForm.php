<?php

namespace common\modules\calculator_service\src\Forms;

use common\modules\calculator_service\src\Interfaces\CalculatorInterface;
use Yii;
use yii\base\Model;

class CalculatorExecuteForm extends Model
{
    public $a;
    public $b;
    public $operation;

    public function rules()
    {
        return [
            [['a', 'b', 'operation'], 'required'],
            [['a', 'b', 'operation'], 'trim'],
            [['a', 'b'], 'number', 'message' => 'Значения должны быть числовыми'],
            [['a', 'b'], 'default', 'value' => 0],
            ['operation', 'in', 'range' => ['+', '-', '*', '/'], 'message' => 'Неверное действие'],
        ];
    }

    public function execute()
    {
        if (!$this->validate()) {
            return false;
        }

        $calc = Yii::$container->get(CalculatorInterface::class); //Yii::createObject

        $result = $calc->calculate($this->operation, $this->a, $this->b);

        return $result;
    }
}