<?php

use common\modules\microservice_communicator\src\Jobs\LaravelJob;

return [
    'definitions' => [
        'TestLaravel' => LaravelJob::class
    ]
];