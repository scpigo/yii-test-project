<?php
namespace common\modules\microservice_communicator\src\Behaviors;

use yii\base\Behavior;
use yii\queue\ExecEvent;
use yii\queue\Queue;
use yii\queue\PushEvent;

class MicroserviceQueueBehavior extends Behavior
{
    public function events()
    {
        return [
            Queue::EVENT_BEFORE_PUSH => 'beforePush',
            Queue::EVENT_AFTER_EXEC => 'afterExec',
            Queue::EVENT_AFTER_ERROR => 'afterError',
        ];
    }

    public function beforePush(PushEvent $event)
    {
        
    }

    public function afterExec(ExecEvent $event)
    {

    }

    public function afterError(ExecEvent $event)
    {
        
    }
}