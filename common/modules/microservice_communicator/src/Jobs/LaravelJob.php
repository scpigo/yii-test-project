<?php

namespace common\modules\microservice_communicator\src\Jobs;

use yii;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\queue\JobInterface;

class LaravelJob extends BaseObject implements JobInterface
{
    public $title;
    public $body;

    public function execute($queue)
    {
        echo($this->title. '   ' .$this->body. "\n");
    }
}