<?php

namespace common\modules\microservice_communicator\src;

use yii;

class Module extends \yii\base\Module implements \yii\base\BootstrapInterface
{
    //public $controllerNamespace = 'common\modules\microservice_communicator\src\Controllers';

    public function init()
    {
        parent::init();
    }

    public function bootstrap($app)
    {
        /* if ($app instanceof \yii\console\Application) {
        	$this->controllerNamespace = 'common\modules\microservice_communicator\src\Commands';
        } */

        $container = require __DIR__. '/./../config/di.php';
        Yii::$container->setDefinitions($container['definitions']);
    }
}
