<?php

namespace common\modules\system_job\src\Jobs;

use yii;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\queue\JobInterface;

class TextJob extends BaseObject implements JobInterface
{
    public $jobId;
    public $jobVars;

    public function execute($queue)
    {
        $text = ArrayHelper::getValue($this->jobVars, 'text');
        $file = Yii::$app->basePath . ArrayHelper::getValue($this->jobVars, 'path');

        file_put_contents($file, 'jobID: '.$this->jobId.'   '.$text."\n", FILE_APPEND);
    }
}