<?php
namespace common\modules\system_job\src\Behaviors;

use common\modules\system_job\src\Helpers\SystemJobStatus;
use yii\base\Behavior;
use yii\queue\ExecEvent;
use yii\queue\Queue;
use common\modules\system_job\src\Models\SystemJob;
use yii\queue\PushEvent;

class JobQueueBehavior extends Behavior
{
    public function events()
    {
        return [
            Queue::EVENT_BEFORE_PUSH => 'beforePush',
            Queue::EVENT_AFTER_EXEC => 'afterExec',
            Queue::EVENT_AFTER_ERROR => 'afterError',
        ];
    }

    public function beforePush(PushEvent $event)
    {
        SystemJob::updateAll([
            'status' => SystemJobStatus::PUSH,
        ], [
            'event_id' => $event->id
        ]);
    }

    public function afterExec(ExecEvent $event)
    {
        SystemJob::updateAll([
            'attempt' => $event->attempt, 
            'executed_at' => gmdate('Y-m-d H:i:s'),
            'status' => SystemJobStatus::EXECUTED,
        ], [
            'event_id' => $event->id
        ]);
    }

    public function afterError(ExecEvent $event)
    {
        SystemJob::updateAll([
            'attempt' => $event->attempt, 
            'executed_at' => gmdate('Y-m-d H:i:s'),
            'status' => SystemJobStatus::FAILED,
        ], [
            'event_id' => $event->id
        ]);
    }
}