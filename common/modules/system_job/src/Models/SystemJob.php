<?php

namespace common\modules\system_job\src\Models;

use yii;
use yii\db\ActiveRecord;

class SystemJob extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%system_jobs}}';
    }

    public function rules()
    {
        return [
            [['action', 'scheduled_at'], 'required'],
        ];
    }
}
