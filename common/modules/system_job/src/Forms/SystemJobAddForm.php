<?php

namespace common\modules\system_job\src\Forms;

use common\modules\system_job\src\Helpers\SystemJobStatus;
use Yii;
use yii\base\Model;
use common\modules\system_job\src\Models\SystemJob;

class SystemJobAddForm extends Model
{
    public $action;
    public $action_vars;
    public $scheduled_at;

    public function rules()
    {
        return [
            [['action', 'action_vars', 'scheduled_at'], 'required'],
            ['action', 'string', 'max' => 100],
            ['action', 'string', 'max' => 1000],
            ['scheduled_at', 'date', 'format' => 'php:Y-m-d H:i:s'],
        ];
    }

    public function addJob()
    {
        if (!$this->validate()) {
            return false;
        }

        $systemJob = new SystemJob();

        $systemJob->action = $this->action;
        $systemJob->action_vars = $this->action_vars;
        $systemJob->scheduled_at = $this->scheduled_at;
        $systemJob->created_at = gmdate('Y-m-d H:i:s');
        $systemJob->status = SystemJobStatus::SCHEDULED;

        $result = $systemJob->save();

        if ($result)
        {
            return $systemJob->id;
        }
        return false;
    }
}