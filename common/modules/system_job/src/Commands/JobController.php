<?php
namespace common\modules\system_job\src\Commands;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use common\modules\system_job\src\Models\SystemJob;
use common\modules\system_job\src\Jobs\TextJob;
use yii\queue\amqp_interop\Queue;
use yii\queue\ExecEvent;
use yii\queue\JobInterface;
use yii\queue\PushEvent;

class JobController extends Controller
{
    public function actionQueue()
    {
        $jobs = SystemJob::find()
            ->where(['event_id' => null])
            ->andWhere(['<', 'scheduled_at', gmdate('Y-m-d H:i:s')])
            ->orderBy('id')
            ->all();
        
        foreach ($jobs as $job) {
            $event_id = null;

            $task = Yii::$container->get($job->action, [], [
                'jobId' => $job->id,
                'jobVars' => json_decode($job->action_vars),
            ]);

            if ($task instanceof JobInterface) {
                $event_id = Yii::$app->queueSystem->push($task);
            }

            if ($event_id) {
                SystemJob::updateAll(['event_id' => $event_id], ['id' => $job->id]);
            }
            
        }
        exit;
    }
}
