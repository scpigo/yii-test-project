<?php

namespace common\modules\system_job\src\Request;

class SystemJobRequest
{
    public $action;
    public $attempt;
    public $status;
}
