<?php

namespace common\modules\system_job\src\Interfaces;

use common\modules\system_job\src\Request\SystemJobRequest;

interface SystemJobRepositoryInterface
{
    public function findModelsByFilter(SystemJobRequest $request);
    public function findIds(SystemJobRequest $request);
}
