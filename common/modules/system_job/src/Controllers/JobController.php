<?php

namespace common\modules\system_job\src\Controllers;


use Yii;
use yii\web\Controller;
use common\modules\system_job\src\Forms\SystemJobAddForm;
use common\modules\system_job\src\Interfaces\SystemJobRepositoryInterface;
use common\modules\system_job\src\Request\SystemJobRequest;

class JobController extends Controller
{
    Public $enableCsrfValidation = false;

    public function actionAdd() {
        $model = new SystemJobAddForm();

		if ($model->load(Yii::$app->request->post(), '') && $id = $model->addJob())
        { 
            return $this->asJson([
                'data' => [
                    'id' => $id,
                ],
                'statusText' => 'Задача запланирована успешно',
                'status' => 'ok'
            ]);
        } 
        return $this->asJson([
            'data' => $model->getErrors(),
            'statusText' => 'Произошла ошибка',
            'status' => 'error'
        ]);
    }

    public function actionFind() {
        $request = new SystemJobRequest();
        $request->status = ['EXECUTED'];

        $repository = Yii::$container->get(SystemJobRepositoryInterface::class); // Yii::createObject
        $ids = $repository->findModelsByFilter($request);
        return $ids;
    }
}
