<?php

namespace common\modules\system_job\src;

use yii;

class Module extends \yii\base\Module implements \yii\base\BootstrapInterface
{
    public $controllerNamespace = 'common\modules\system_job\src\Controllers';

    public function init()
    {
        parent::init();
    }

    public function bootstrap($app)
    {
        if ($app instanceof \yii\console\Application) {
        	$this->controllerNamespace = 'common\modules\system_job\src\Commands';
        }

        $container = require __DIR__. '/./../config/di.php';
        Yii::$container->setDefinitions($container['definitions']);
    }
}
