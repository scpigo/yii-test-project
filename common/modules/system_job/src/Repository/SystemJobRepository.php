<?php

namespace common\modules\system_job\src\Repository;

use common\modules\system_job\src\Interfaces\SystemJobRepositoryInterface;
use common\modules\system_job\src\Models\SystemJob;
use common\modules\system_job\src\Request\SystemJobRequest;

class SystemJobRepository implements SystemJobRepositoryInterface
{
    public function findModelsByFilter(SystemJobRequest $request) {
        return SystemJob::find()->andFilterWhere(['in', 'action', $request->action])
        ->andFilterWhere(['in', 'attempt', $request->attempt])
        ->andFilterWhere(['in', 'status', $request->status])->all();
    }

    public function findIds(SystemJobRequest $request) {
        return SystemJob::find()->select('id')->andFilterWhere(['in', 'action', $request->action])
        ->andFilterWhere(['in', 'attempt', $request->attempt])
        ->andFilterWhere(['in', 'status', $request->status])->column();
    }
}
