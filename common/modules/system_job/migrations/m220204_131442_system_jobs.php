<?php

use yii\db\Migration;

/**
 * Class m220204_131442_system_jobs
 */
class m220204_131442_system_jobs extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%system_jobs}}', [
            'id' => $this->primaryKey(),
            'action' => $this->string(100)->notNull(),
            'action_vars' => $this->string(1000)->notNull(),
            'scheduled_at' => $this->dateTime()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'executed_at' => $this->dateTime(),
            'attempt' => $this->integer(),
            'event_id' => $this->string(100),
            'status' => $this->string(100)->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%system_jobs}}');
    }
}
