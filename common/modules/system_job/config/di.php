<?php

use common\modules\system_job\src\Jobs\TextJob;
use common\modules\system_job\src\Interfaces\SystemJobRepositoryInterface;
use common\modules\system_job\src\Repository\SystemJobRepository;

return [
    'definitions' => [
        SystemJobRepositoryInterface::class => SystemJobRepository::class,
        'TEXT_RANDOM' => TextJob::class
    ]
];