<?php

namespace common\models\search;

use yii\data\ActiveDataProvider;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use common\models\PostsMetrics;

class PostMetricsSearch extends Model
{
    public $group_type;
    public $page;
    public $pageSize;
    public $sort;
    public $sortType;

    public function rules()
    {
        return [
            [['page', 'pageSize'], 'safe'],
            [['group_type', 'sort', 'sortType'], 'string', 'max' => 32],
            [['page'], 'default', 'value' => 0],
            [['pageSize'], 'default', 'value' => 10],
            [['sort'], 'default', 'value' => 'views'],
            ['sort', 'in', 'range' => ['post_id', 'views', 'likes', 'comments', 'reposts'], 'message' => 'Указано неверное значение поля сортировки'],
            [['sortType'], 'default', 'value' => 'asc'],
            ['sortType', 'in', 'range' => ['asc', 'desc'], 'message' => 'sortType должен быть равным asc или desc'],
        ];
    }

    public function search()
    {
        if (!$this->validate()) {
            return false;
        }

        $query = PostsMetrics::find()
            ->andFilterWhere(['group_type' => $this->group_type]);

        $sortType = ($this->sortType == 'asc') ? SORT_ASC : SORT_DESC;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'page' => $this->page,
                'pageSize' => $this->pageSize,
            ],
            'sort' => [
                'defaultOrder' => [
                    $this->sort => $sortType,
                ]
            ],
        ]);

        return $dataProvider;
    }
}