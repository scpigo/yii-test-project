<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class Users extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%users}}';
    }

    public static function findIdentity($user_id)
    {
        return static::findOne(['user_id' => $user_id]);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
		return static::find()
			->where(['user_id' => (string) $token->getClaim('uid')])
			->one();
	}

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function afterSave($isInsert, $changedOldAttributes) {
		if (array_key_exists('password', $changedOldAttributes)) {
			\common\models\UserRefreshToken::deleteAll(['urf_userID' => $this->user_id]);
		}

		return parent::afterSave($isInsert, $changedOldAttributes);
	}
}