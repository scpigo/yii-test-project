<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class SystemSettings extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%system_settings}}';
    }

    public function rules()
    {
        return [
            [['key', 'value'], 'required'],
            [['key', 'value'], 'string', 'max' => 32],
        ];
    }
}