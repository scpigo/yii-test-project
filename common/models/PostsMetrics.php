<?php

namespace common\models;

use Yii;
use yii\mongodb\ActiveRecord;
use yii\behaviors\TimestampBehavior;

class PostsMetrics extends ActiveRecord
{
    public static function collectionName()
    {
        return 'posts_metrics';
    }

    public function rules()
    {
        return [
            [['group_type'], 'required'],
            [['views', 'likes', 'comments', 'reposts'], 'number'],
            [['group_type'], 'string', 'max' => 32],
            ['group_type', 'in', 'range' => ['vk', 'ok', 'fb']],
        ];
    }

    public function attributes()
    {
        return [
            '_id', 
            'post_id', 
            'group_type', 
            'views', 
            'likes', 
            'comments', 
            'reposts', 
            'created_at'
        ];
    }
}