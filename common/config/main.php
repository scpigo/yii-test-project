<?php

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => [
        'queueFile',
        'queueSystem',
        'queueMicroservice'
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'queueFile' => [
            'class' => \yii\queue\amqp_interop\Queue::class,
            'port' => 5672,
            'user' => 'guest',
            'password' => 'guest',
            'queueName' => 'queue',
            'exchangeName' => 'queue-exchange',
            'driver' => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_BUNNY,
            'dsn' => 'amqp://guest:guest@rabbitmq:5672/%2F',
        ],
        'queueSystem' => [
            'class' => \yii\queue\amqp_interop\Queue::class,
            'port' => 5672,
            'user' => 'guest',
            'password' => 'guest',
            'queueName' => 'system',
            'exchangeName' => 'system-exchange',
            'driver' => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_BUNNY,
            'dsn' => 'amqp://guest:guest@rabbitmq:5672/%2F',
            'as jobQueueBehavior' => common\modules\system_job\src\Behaviors\JobQueueBehavior::class,
        ],
        'queueMicroservice' => [
            'class' => \yii\queue\amqp_interop\Queue::class,
            'port' => 5672,
            'user' => 'guest',
            'password' => 'guest',
            'queueName' => 'microservice',
            'exchangeName' => 'microservice-exchange',
            'driver' => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_BUNNY,
            'dsn' => 'amqp://guest:guest@rabbitmq:5672/%2F',
            'strictJobType' => false,
            'serializer' => \yii\queue\serializers\JsonSerializer::class,
            'as microserviceQueueBehavior' => common\modules\microservice_communicator\src\Behaviors\MicroserviceQueueBehavior::class,
        ],
        'mongodb' => [
            'class' => '\yii\mongodb\Connection',
            'dsn' => 'mongodb://root:example@mongo:27017/database',
        ],
        'system_settings' => [
            'class' => 'common\components\SystemSettingsManager',
        ],
    ],
];
