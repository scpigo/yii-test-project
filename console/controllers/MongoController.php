<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;
use yii\mongodb\Query;

class MongoController extends Controller
{
    public function actionGenerate() {
        $groups = array('vk', 'ok', 'fb');
        
        $posts = Yii::$app->mongodb->getCollection('posts_metrics');

        for ($i = 0; $i < 1000; $i++) {
            $lastPost = (new Query())
            ->select(['post_id'])
            ->from('posts_metrics')
            ->orderBy('post_id DESC')
            ->one();
        
            $lastId = 0;

            if ($lastPost != null) $lastId = $lastPost['post_id'];

            $group = array_rand($groups);

            $posts->insert([
                'post_id' => $lastId + 1,
                'group_type' => $groups[$group],
                'views' => rand(1,10000),
                'likes' => rand(0,10000),
                'comments' => rand(0, 100),
                'reposts' => rand(0, 1000),
                'created_at' => gmdate('Y-m-d H:i:s')
            ]);
        }
        exit;
    }
}