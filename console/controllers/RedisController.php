<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use yii\helpers\Console;

class RedisController extends Controller
{

    public function actionCounter() {
        $redis = Yii::$app->redis;
        $redis->executeCommand('incr', ['counter']);
        echo $redis->executeCommand('get', ['counter'])."\n";
        exit;
    }

    public function actionReset() {
        $redis = Yii::$app->redis;
        $redis->executeCommand('del', ['counter']);
        exit;
    }
}

?>