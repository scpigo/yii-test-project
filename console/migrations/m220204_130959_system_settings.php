<?php

use yii\db\Migration;

/**
 * Class m220204_130959_system_settings
 */
class m220204_130959_system_settings extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%system_settings}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(32)->notNull(),
            'value' => $this->string(32)->notNull(),
            'created_at' => $this->dateTime()->notNull()->defaultValue(gmdate('Y-m-d H:i:s')),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%system_settings}}');
    }
}
