<?php

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'queueFile', 
        'log', 
        'system_job',
        'microservice_communicator'
    ],
    'controllerNamespace' => 'console\controllers',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap' => [
        'migrate-system-job' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationPath' => '@common/modules/system_job/migrations',
        ],
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
          ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'modules' => [
        'system_job' => [
            'class' => common\modules\system_job\src\Module::class,
        ],
        'microservice_communicator' => [
            'class' => common\modules\microservice_communicator\src\Module::class,
        ],
    ],
    'params' => $params,
];
